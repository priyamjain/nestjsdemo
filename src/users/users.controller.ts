import { Controller, Get, Param, Post, Body, Put, Delete, HttpException, HttpStatus } from '@nestjs/common';
import { UsersService } from './users.service';
import { UserEntity } from './user.entity';
import { identifier } from '@babel/types';
import { async } from 'rxjs';

@Controller('users')
export class UsersController {

    constructor(private service: UsersService) { }

    @Get(':id')
    get(@Param() params) {
        const result = this.service.getUser(params.id);
        if(! result ) {
        throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
        }
        else{
            return result;
        }
    }

    @Post()
    create(@Body() user: UserEntity) {
        return this.service.createUser(user);
    }

    @Put(':id')
    update(@Body() user: UserEntity,@Param() param) {
        return this.service.updateUser(param.id,user);
    }

    @Delete(':id')
    deleteUser(@Param() params) {
        return this.service.deleteUser(params.id);
    }


}
