import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
@Entity('tbl_user')
export class UserEntity {

    @PrimaryGeneratedColumn()
    userid : number;

    @Column()
    name: string;

    @Column() 
    emailid: string;


}
