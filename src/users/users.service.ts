import { Injectable, Inject, Body } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, InsertResult } from 'typeorm';
import { UserEntity } from './user.entity';


@Injectable()
export class UsersService {
    UserEntity: any;

    constructor(@InjectRepository(UserEntity) private usersRepository: Repository<UserEntity>) { }

    async getUsers(user: UserEntity): Promise<UserEntity[]> {
        return await this.usersRepository.find();
    }

    async getUser(_id: number): Promise<UserEntity> {
        return await this.usersRepository.findOne(_id);
    }


    async updateUser(id: number,user: UserEntity) {
        this.usersRepository.update(id,user)
    }

    async deleteUser(user: UserEntity) {
        this.usersRepository.delete(user);
    }
    async createUser(user: UserEntity)  {

        this.UserEntity.query("call sp_userdetail(?,?)", [user.name, user.emailid]);
      return await this.usersRepository.insert(user);
    }
}
